const express = require('express');
const cors = require('cors');

const { onlyAllowGET } = require('./middlewares');
const { routesSTUR } = require('./stur');

const app = express();
const port = process.env.PORT || 3000;

app.use(express.urlencoded({ extended: true }));
app.use(express.json());
app.use(cors());
app.use(function(req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  res.header('Access-Control-Allow-Methods', '*');
  next();
});
app.use('/', express.Router(), onlyAllowGET, routesSTUR);
app.listen(port, () => {
  console.log(`Servidor executando na porta ${port}`)
});

module.exports = { app };