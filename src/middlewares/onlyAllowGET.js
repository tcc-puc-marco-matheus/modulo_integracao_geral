const { constants } = require('../shared');

const onlyAllowGET = (req, res, next) => {
  switch (req.method) {
    case 'GET':
        next();
        break;

    default:
        res.status(401);
        res.send({ ...constants.responseStatus[401] });
        break;
    }
};

module.exports = onlyAllowGET;