const Enum = require('enum');

const responseStatus = new Enum({
  200: { message: 'Ok' },
  201: { message: 'Criado com sucesso' },
  204: { message: 'Sem conteúdo' },
  400: { message: 'Formato requisição inválido' },
  401: { message: 'Não autorizado' },
  404: { message: 'Rota não existe' },
  422: { message: 'Não foi possível realizar operação' },
  500: { message: 'Erro interno' }
}).toJSON();

module.exports = { responseStatus };