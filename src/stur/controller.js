const getIPTUByCpf = (cpf) => {
    const tax = calTax(500, 5000);
    return {
        CPF: `${cpf}`,
        IPTU: `${tax}`
    };
};

const getIPTUByCnpj = (cnpj) => {
    const tax = calTax(1000, 10000);
    return {
        CNPJ: `${cnpj}`,
        IPTU: `${tax}`
    };
};

const getITR = (cpf) => {    
    const tax = calTax(5000, 50000);
    return {
        CPF: `${cpf}`,
        ITR: `${tax}`
    };
};

const calTax = (min, max) => {
    return Math.floor(Math.random() * (max - min) + min);
};

module.exports = {
    getIPTUByCpf,
    getIPTUByCnpj,
    getITR
};
