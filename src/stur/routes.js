const express = require("express");

const controllerSTUR = require("./controller");

const routesSTUR = express.Router();

routesSTUR.get('/stur/iptu/cpf/:cpf', (req, res) => {
  try {
    const cpf = req.params.cpf;
    const ret = controllerSTUR.getIPTUByCpf(cpf);
    res.status(200).json(ret);
  } catch (error) {
    res.status(500).json(error);
  }
})

routesSTUR.get('/stur/iptu/cnpj/:cnpj', (req, res) => {
  try {
    const cnpj = req.params.cnpj;
    const ret = controllerSTUR.getIPTUByCnpj(cnpj);
    res.status(200).json(ret);
  } catch (error) {
    res.status(500).json(error);
  }
})

routesSTUR.get('/stur/itr/:cpf', (req, res) => {
  try {
    const cpf = req.params.cpf;
    const ret = controllerSTUR.getITR(cpf);
    res.status(200).json(ret);
  } catch (error) {
    res.status(500).json(error);
  }
})

module.exports = routesSTUR;