const onlyAllowGET = require('./onlyAllowGET');

module.exports = {
  onlyAllowGET
};